package com.enjoyplay.listener;

import com.enjoyplay.bean.City;

public interface InnerListener {
    void dismiss(int position, City data);
    void locate();
}
