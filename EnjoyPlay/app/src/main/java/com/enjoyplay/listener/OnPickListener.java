package com.enjoyplay.listener;

import com.enjoyplay.bean.City;

public interface OnPickListener {
    void onPick(int position, City data);
    void onLocate();
}
