package com.enjoyplay.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.Constants;
import com.enjoyplay.api.API;
import com.enjoyplay.bean.City;
import com.enjoyplay.bean.LocateState;
import com.enjoyplay.bean.LocatedCity;
import com.enjoyplay.enjoyplay.R;
import com.enjoyplay.listener.OnPickListener;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener{

    private TextView mAddressText;
    private SearchView mSearchView;
    private ImageView mMessageImg;


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initView(view);
        loadData();
        return view;
    }

    private void initView(View view) {
        mAddressText = view.findViewById(R.id.address);
//        mSearchView = view.findViewById(R.id.searchView);
        mMessageImg = view.findViewById(R.id.messageImg);
    }




    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).
                addCallAdapterFactory(RxJavaCallAdapterFactory.create()).baseUrl(Constants.BASE_URL).build();
        API mApi = retrofit.create(API.class);
//        List<City> cities = MyApplication.getDaoSession().getCityDao().loadAll();
//        Log.d("lodataData", cities.size() + "lodataData");
//        for (int i = 0; i < cities.size(); i++) {
//            Log.d("loadData", cities.get(i).toString());
//        }
//        mApi.getCityList().subscribeOn(Schedulers.io()).subscribe();
//        mApi.getCity().subscribeOn(Schedulers.io()).subscribe(new Subscriber<ResponseBody>() {
//            @Override
//            public void onCompleted() {
//                Log.i("zzz", "onCompleted");
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                Log.i("zzz", e.getMessage());
//            }
//
//            @Override
//            public void onNext(ResponseBody responseBody) {
//                try {
//                    Log.i("zzz", responseBody.string());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address:
                break;
            case R.id.searchView:
                break;
            case R.id.messageImg:
                break;
        }
    }
}
