package com.enjoyplay.api;

import com.enjoyplay.bean.City;

import com.enjoyplay.bean.Hotel;
import com.enjoyplay.bean.HotelDetail;
import com.enjoyplay.bean.Response;
import com.enjoyplay.bean.Scenery;
import com.enjoyplay.bean.SceneryDetail;
import com.enjoyplay.bean.TicketInfo;

import retrofit2.http.Field;
import retrofit2.http.GET;
import rx.Observable;

public interface API {

    @GET("cityList?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<City>> getCityList();

    @GET("scenery?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<Scenery>> getSceneryList(@Field("pid") int pid, @Field("cid") int cid, @Field("page") int page);

    @GET("GetScenery?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<SceneryDetail>> getSceneryDetailList(@Field("sid") int sid);

    @GET("TicketInfo?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<TicketInfo>> getTicketInfoList(@Field("sid") int sid);

    @GET("HotelList?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<Hotel>> getHotelList(@Field("cityid") int cityid);

    @GET("GetHotel?key=df832b5661df4e23969efce514ae915b")
    Observable<Response<HotelDetail>> getHotelDetailList(@Field("hid") int hid);
}
