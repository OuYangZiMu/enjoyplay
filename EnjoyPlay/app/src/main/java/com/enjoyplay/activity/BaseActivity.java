package com.enjoyplay.activity;

import android.app.Activity;
import android.os.Bundle;

import java.util.Stack;

public abstract class BaseActivity extends Activity {
    static Stack<Activity> activities = new Stack<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activities.add(this);
        initView();
        initData();
        initListener();
    }

    public abstract void initView();

    public abstract void initData();

    public abstract void initListener();

    public static void finishActivity(){
        for(Activity x : activities){
            if(x != null){
                x.finish();
            }
        }
        System.exit(0);
    }
}
