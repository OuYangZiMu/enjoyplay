package com.enjoyplay.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.enjoyplay.bean.City;
import com.enjoyplay.bean.HotCity;
import com.enjoyplay.bean.LocateState;
import com.enjoyplay.bean.LocatedCity;
import com.enjoyplay.enjoyplay.BuildConfig;
import com.enjoyplay.enjoyplay.R;
import com.enjoyplay.fragment.CityPicker;
import com.enjoyplay.fragment.FindFragment;
import com.enjoyplay.fragment.MainFragment;
import com.enjoyplay.fragment.MyFragment;
import com.enjoyplay.fragment.ShareFragment;
import com.enjoyplay.fragment.ShopFragment;
import com.enjoyplay.listener.OnPickListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String ACTION_RECEIVE_Main = BuildConfig.APPLICATION_ID + "-ACTION_RECEIVE_Main";
    public static final String ACTION_RECEIVE_Shop = BuildConfig.APPLICATION_ID + "-ACTION_RECEIVE_Shop";
    public static final String ACTION_RECEIVE_Find = BuildConfig.APPLICATION_ID + "-ACTION_RECEIVE_Find";
    public static final String ACTION_RECEIVE_Share = BuildConfig.APPLICATION_ID + "-ACTION_RECEIVE_Share";
    public static final String ACTION_RECEIVE_My = BuildConfig.APPLICATION_ID + "-ACTION_RECEIVE_My";
    private FrameLayout mFrameLayout;
    private View mNaviBar;
    private View mMainView;
    private View mShopView;
    private View mFindView;
    private View mShareView;
    private View mMyView;

    private List<String> mTagList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setContentFragment(ACTION_RECEIVE_Main);
    }

    private void initView() {
        mFrameLayout = findViewById(R.id.fragment_Content);
        mNaviBar = findViewById(R.id.naviBar);
        mMainView = findViewById(R.id.mainView);
        mMainView.setOnClickListener(this);
        mShopView = findViewById(R.id.shop);
        mShopView.setOnClickListener(this);
        mFindView = findViewById(R.id.find);
        mFindView.setOnClickListener(this);
        mShareView = findViewById(R.id.share);
        mShareView.setOnClickListener(this);
        mMyView = findViewById(R.id.my);
        mMyView.setOnClickListener(this);
    }

    private void setContentFragment(String extra) {
        mNaviBar.setVisibility(View.VISIBLE);
        Fragment fragment = null;
        if (extra == null) {
            finish();
            return;
        } else if (extra.equals(ACTION_RECEIVE_Main)) {
            fragment = new MainFragment();
        } else if (extra.equals(ACTION_RECEIVE_Share)) {
            fragment = new ShareFragment();
        } else if (extra.equals(ACTION_RECEIVE_Find)) {
            fragment = new FindFragment();
        } else if (extra.equals(ACTION_RECEIVE_Shop)) {
            fragment = new ShopFragment();
        } else if (extra.equals(ACTION_RECEIVE_My)) {
            fragment = new MyFragment();
        }
        commitFragment(fragment, extra);
        setSelectIcon(extra);
    }

    void commitFragment(Fragment targetFragment, String tag) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment activeFragment = getFragmentManager().findFragmentByTag(tag);
        mTagList.add(tag);
        if (activeFragment == null) {
            activeFragment = targetFragment;
        }
        ft.replace(R.id.fragment_Content, targetFragment, tag);
        ft.addToBackStack(tag);
        ft.commitAllowingStateLoss();
    }

    private void setSelectIcon(String tag) {
        if (tag.equals(ACTION_RECEIVE_Main)) {
            mMainView.setSelected(true);
            mFindView.setSelected(false);
            mShopView.setSelected(false);
            mShareView.setSelected(false);
            mMyView.setSelected(false);
        } else if (tag.equals(ACTION_RECEIVE_Find)) {
            mMainView.setSelected(false);
            mFindView.setSelected(true);
            mShopView.setSelected(false);
            mShareView.setSelected(false);
            mMyView.setSelected(false);
        } else if (tag.equals(ACTION_RECEIVE_Shop)) {
            mMainView.setSelected(false);
            mFindView.setSelected(false);
            mShopView.setSelected(true);
            mShareView.setSelected(false);
            mMyView.setSelected(false);
        } else if (tag.equals(ACTION_RECEIVE_Share)) {
            mMainView.setSelected(false);
            mFindView.setSelected(false);
            mShopView.setSelected(false);
            mShareView.setSelected(true);
            mMyView.setSelected(false);
        } else if (tag.equals(ACTION_RECEIVE_My)) {
            mMainView.setSelected(false);
            mFindView.setSelected(false);
            mShopView.setSelected(false);
            mShareView.setSelected(false);
            mMyView.setSelected(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mainView:
                if (!mMainView.isSelected()) {
                    setContentFragment(ACTION_RECEIVE_Main);
                }
                break;
            case R.id.find:
                if (!mFindView.isSelected()) {
                    setContentFragment(ACTION_RECEIVE_Find);
                }
                break;
            case R.id.shop:
                if (!mShopView.isSelected()) {
                    setContentFragment(ACTION_RECEIVE_Shop);
                }
                break;
            case R.id.share:
                if (!mShareView.isSelected()) {
                    setContentFragment(ACTION_RECEIVE_Share);
                }
                 break;
            case R.id.my:
                if (!mMyView.isSelected()) {
                    setContentFragment(ACTION_RECEIVE_My);
                }
                break;


        }
    }

    private void setCityPicker() {
        List<HotCity> hotCities = new ArrayList<>();
        CityPicker.getInstance()
                .setFragmentManager(getSupportFragmentManager())
                .enableAnimation(true)
                .setAnimationStyle(R.style.DefaultCityPickerAnimation)
                .setLocatedCity(null)
                .setHotCities(hotCities)
                .setOnPickListener(new OnPickListener() {
                    @Override
                    public void onPick(int position, City data) {
//                        currentTV.setText(data == null ? "杭州" : String.format("当前城市：%s，%s", data.getCityName(), data.getCityId()));
                        if (data != null) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    String.format("点击的数据：%s，%s", data.getCityName(), data.getCityId()),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }

                    @Override
                    public void onLocate() {
                        //开始定位，这里模拟一下定位
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                CityPicker.getInstance().locateComplete(new LocatedCity("深圳", "广东", "101280601"), LocateState.SUCCESS);
                            }
                        }, 3000);
                    }
                })
                .show();
    }
}
