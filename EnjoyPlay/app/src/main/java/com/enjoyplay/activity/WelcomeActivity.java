package com.enjoyplay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.Constants;
import com.enjoyplay.api.API;
import com.enjoyplay.app.MyApplication;
import com.enjoyplay.bean.City;
import com.enjoyplay.bean.Response;
import com.enjoyplay.enjoyplay.R;
import com.enjoyplay.manger.SharePreManager;
import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * 欢迎界面
 */

public class WelcomeActivity extends BaseActivity implements Animation.AnimationListener {
    ImageView imageview_welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_welcome);
        imageview_welcome = findViewById(R.id.imageview_welcome);
    }

    @Override
    public void initData() {

    }

    @Override
    public void initListener() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_welcome);
        animation.setDuration(3000);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        imageview_welcome.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {
//        if (!SharePreManager.getInstance(this).getShafArea()) {
            loadCityInfo();
//        } else {
//            Log.d("WelcomeActivity", "地区数据库已装载。");
//        }
    }

    //动画结束页面转换
    @Override
    public void onAnimationEnd(Animation animation) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void loadCityInfo() {
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).
                addCallAdapterFactory(RxJavaCallAdapterFactory.create()).baseUrl(Constants.BASE_URL).build();
        API mApi = retrofit.create(API.class);
        mApi.getCityList().subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new Subscriber<Response<City>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response<City> cityResponse) {
                Log.d("WelcomeActivity", cityResponse.toString());
                List<City> list = new ArrayList<>();
                if (cityResponse.getError_code() == 0) {
                    for (int i = 0; i < cityResponse.getResult().size(); i++) {
                        City city = cityResponse.getResult().get(i);
                        city.setNamePinYin(Pinyin.toPinyin(city.getCityName(), ""));
                        list.add(city);
                    }
                    Log.d("WelcomeActivity", list.size() + "Size");
                    MyApplication.getDaoSession().getCityDao().saveInTx(list);
                    List<City> cities = MyApplication.getDaoSession().getCityDao().queryBuilder().list();
                    Log.d("WelcomeActivity", cities.size() + "Size");
                    SharePreManager.getInstance(WelcomeActivity.this).updateShafArea(true);
                }
            }
        });
    }
}
