package com.enjoyplay.manger;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreManager {
    public static String PREFERENCES_NAME = "local_info";
    public final String Key_SHAF_AREA = "PROVINCE_LOADED_YET";
    private static SharePreManager sPreManager;
    private static SharedPreferences sSharedPre;

    private SharePreManager() {
    }

    public static SharePreManager getInstance(Context context) {
        synchronized (SharePreManager.class) {
            if (sSharedPre == null) {
                sPreManager = new SharePreManager();
                sSharedPre = context.getSharedPreferences(PREFERENCES_NAME,
                        Context.MODE_PRIVATE);
            }
        }
        return sPreManager;
    }


    public void updateShafArea(boolean status) {
        SharedPreferences.Editor editor = sSharedPre.edit();
        editor.putBoolean(Key_SHAF_AREA, status);
        editor.apply();
        editor.clear();
    }

    public boolean getShafArea() {
        return sSharedPre.getBoolean(Key_SHAF_AREA, false);
    }
}
