package com.enjoyplay.app;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.enjoyplay.db.DaoMaster;
import com.enjoyplay.db.DaoSession;

public class MyApplication extends Application {
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase mDb;
    private DaoMaster mDaoMaster;
    private static DaoSession mDaoSession;

    public MyApplication() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
    }

    private void setupDatabase() {
        mHelper = new DaoMaster.DevOpenHelper(this, "enjoy_play", null);
        mDb = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(mDb);
        mDaoSession = mDaoMaster.newSession();
    }

    public static DaoSession getDaoSession() {
        return mDaoSession;
    }

    public SQLiteDatabase getDb() {
        return mDb;
    }
}
