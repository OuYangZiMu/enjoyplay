package com.enjoyplay.bean;

public class Hotel {
    private String id;
    private String name;
    private String className;
    private String intro;
    private String dpNum;
    private String Lat;
    private String Lon;
    private String address;
    private String largePic;
    private String cityId;
    private String url;
    private String manyidu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDpNum() {
        return dpNum;
    }

    public void setDpNum(String dpNum) {
        this.dpNum = dpNum;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLon() {
        return Lon;
    }

    public void setLon(String lon) {
        Lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLargePic() {
        return largePic;
    }

    public void setLargePic(String largePic) {
        this.largePic = largePic;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getManyidu() {
        return manyidu;
    }

    public void setManyidu(String manyidu) {
        this.manyidu = manyidu;
    }
}
