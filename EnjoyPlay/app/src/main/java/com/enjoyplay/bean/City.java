package com.enjoyplay.bean;

import android.text.TextUtils;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
public class City {
    private String cityId;
    private String cityName;
    private String provinceId;
    private String namePinYin;

    @Generated(hash = 404352531)
    public City(String cityId, String cityName, String provinceId, String namePinYin) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.provinceId = provinceId;
        this.namePinYin = namePinYin;
    }

    @Generated(hash = 750791287)
    public City() {
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getNamePinYin() {
        return namePinYin;
    }

    public void setNamePinYin(String namePinYin) {
        this.namePinYin = namePinYin;
    }

    //获得悬浮框文字
    public String getSection(){
        if (TextUtils.isEmpty(namePinYin)) {
            return "#";
        } else {
            String c = namePinYin.substring(0, 1);
            Pattern p = Pattern.compile("[a-zA-Z]");
            Matcher m = p.matcher(c);
            if (m.matches()) {
                return c.toUpperCase();
            }

            else if (TextUtils.equals(c, "定") || TextUtils.equals(c, "热"))
                return namePinYin;
            else
                return "#";
        }
    }

        @Override
    public String toString() {
        return "cityId:" + cityId + "/n" + "cityName:" + cityName + "/n" + "provinceId:" + provinceId;
    }
}
