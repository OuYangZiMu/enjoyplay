package com.enjoyplay.bean;

public class LocatedCity extends City {

    public LocatedCity(String name, String province, String code) {
        super(name, province, code, "定位城市");
    }
}
