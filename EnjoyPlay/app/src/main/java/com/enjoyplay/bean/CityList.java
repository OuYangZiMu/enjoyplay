package com.enjoyplay.bean;

import java.util.List;

public class CityList {
    private int error_code;
    private String reason;
    private List<City> result;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<City> getResult() {
        return result;
    }

    public void setResult(List<City> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "error_code:" + error_code + "/n" + "reason:" + reason + "/n" + "result:" + result.toString();
    }
}

