package com.enjoyplay.bean;

public class HotCity extends City {

    public HotCity(String name, String province, String code) {
        super(name, province, code ,"热门城市");
    }
}
